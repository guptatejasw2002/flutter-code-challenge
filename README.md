# flutter-code-challenge

To complete this challenge, please replicate **one** of the following designs using Flutter v2 or greater:

![img1](1cdd0c877867bd960c60b32d06553db6.webp)
![img2](3dfa8b006a875ad98c22e37b8e5e0cd0.webp)
![img3](4.webp)
![img4](79e07ff74f4c9b444ffae8515071524b.webp)
![img5](ad20f4abaa2275923a79fa5540908eb2.webp)
![img6](af25e495dbba6699fe372deca7e102db.webp)
![img7](atach_4x.webp)

Challenge requirements:

- Clone the UI from a design image using real Flutter Widgets as if you were going to create an app from this. Don't worry about using the exact same images or icons, we'll accept alternates as long as the UI components are similar to the original. We're not concerned with the navigation or on-tap responses from all components, but all fields/inputs, and dropdowns should function.
- The application you create must be compilable using Flutter version 2, and be able to run on either Flutter for mobile, Flutter Web or Flutter Desktop. Any submissions that cannot be compiled and run are considered an automatic fail.
- Record your progress using Git. Your submission must be either shared as a public Git repo (Github, Gitlab, Bitbucket etc) or a GitBundle file. We'll be looking to see how you work within Git, so please utilize as you would in a work scenario.
- Complete this challenge in less than a week. We'll be using the Git history to confirm when you started the project.

Notes:

- We'll accept all use of the Flutter project template
- No animations, state management or native integrations required, anything you add here will be considered extra credit.
- We're looking for completeness of code, not completeness of the design you pick.
- Any reasonable and useful documentation will always be appreciated.
- If short on time, please complete the portion of the design and turn in as is. We'd prefer a partially complete design with a few good components rather than a more complete design where none of the components are done.

When you've completed the project and are ready to submit, please send either a link to your Git repo, or a GitBundle file to tejasw@therecspot.com. We'll review the submission and follow up with you.

Feel free to send any questions to tejasw@therecspot.com. Have fun!
